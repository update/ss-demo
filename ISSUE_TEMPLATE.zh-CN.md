## 该问题是怎么引起的？
1、不能通过http://127.0.0.1:10000/ssDemo/访问主页
只能通过http://localhost:10000/ssDemo/或http://192.168.10.133:10000/ssDemo/来访问主页

原因：这是由于端口被其他程序占用导致的
使用tomcat7-maven-plugin时，不会发现端口占用的问题，它认为localhost(默认虚拟地址)和127.0.0.1（主机）是不同的。
所以使用localhost是虚拟主机地址，可以正常访问，而127.0.0.1代表的真正主机地址，端口占用时访问肯定会不成功。

解决方法：
web程序使用其他端口号，或关掉占用端口的程序。
查找并关闭端口对应程序：
在命令提示符窗口中输入netstat -ano回车，找到TCP 127.0.0.1:10000端口对应的PID,
然后根据此 PID在任务管理器里找到对应的程序名，就可知道是什么程序占用了端口，进而查明程序的来源。

2、java.io.FileNotFoundException: Could not open ServletContext resource [/WEB-INF/SpringMVC-servlet.xml]
原因：spring mvc的配置文件未加载到。
Spring MVC的默认配置文件是/WEB-INF/servlet-name + servlet.xml，即/WEB-INF/SpringMVC-servlet.xml。
    <!-- Spring MVC servlet -->
    <servlet>
        <servlet-name>SpringMVC</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <!-- <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>
                /WEB-INF/config/spring-mvc-servlet.xml
            </param-value>
        </init-param> -->
        <load-on-startup>1</load-on-startup>
        <async-supported>true</async-supported>
    </servlet>
    <servlet-mapping>
        <servlet-name>SpringMVC</servlet-name>
        <!-- 此处可以配置成*.do，对应struts的后缀习惯 -->
        <url-pattern>/</url-pattern>
    </servlet-mapping>


解决方法：
可以通过init-param来指定Spring MVC的配置文件：
    <init-param>
         <param-name>contextConfigLocation</param-name>
         <param-value>
              /WEB-INF/config/spring-mvc-servlet.xml
         </param-value>
    </init-param>
也可以将配置文件名修改为/WEB-INF/SpringMVC-servlet.xml,或者将SpringMVC(serlvet-name)改为spring-mvc。
    
3、java.io.FileNotFoundException: Could not open ServletContext resource [/WEB-INF/applicationContext.xml]
原因：spring的配置文件未加载到。

解决方法：
Spring的默认配置文件是/WEB-INF/applicationContext.xml，
当web.xml中的context-param中未指定Spring配置文件时，会默认去加载/WEB-INF/applicationContext.xml。 
    
    
## 重现步骤
http://127.0.0.1:10000/ssDemo/

## 报错信息
使用其他程序占用的端口号，不会报错，而无法访问资源。Network下显示资源一直处于pending（等待）状态
Provisional headers are shown


