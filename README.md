#ss-demo
springmvc + spring security 权限控制示例


一般来说，Web 应用的安全性包括用户认证（Authentication）和用户授权（Authorization）两个部分。
其中，用户认证指的是验证某个用户是否为系统中的合法主体，也就是说用户能否访问该系统。
用户认证一般要求用户提供用户名和密码。系统通过校验用户名和密码来完成认证过程。
用户授权指的是验证某个用户 是否有权限执行某个操作。在一个系统中，不同用户所具有的权限是不同的。
比如对一个文件来说，有的用户只能进行读取，而有的用户可以进行修改。
一般来说， 系统会为不同的用户分配不同的角色，而每个角色则对应一系列的权限。

 对于上面提到的两种应用情景，Spring Security 框架都有很好的支持。
 Spring Security 基于 Spring 框架，提供了一套 Web 应用安全性的完整解决方案：
 在用户认证方面，Spring Security 框架支持主流的认证方式，包括 HTTP 基本认证、HTTP 表单验证、HTTP 摘要认证、OpenID 和 LDAP 等。
 在用户授权方面，Spring Security 提供了基于角色的访问控制和访问控制列表（Access Control List，ACL），可以对应用中的领域对象进行细粒度的控制。